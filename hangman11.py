import random
from turtle import *


#hangman drawing
counter = 0

def hbody(right, forward, back):
    right(right)

    # Draw a line
    forward(forward)

    # Go back to the center
    back(back)

def hhead(left, circle, right):
    # Draw a circle for a head
    left(left)
    circle(circle)
    right(right)

def hrarm(forward1, right, forward2, back):
    # Move down the line
    forward(forward)

    # Draw right arm
    right(right)
    forward(forward2)
    back(back)

def hlarm(left, forward, back):
    # Draw left arm
    left(left)
    forward(forward)
    back(back)

def hlleg(right, forward1, left, forward2, back):
    # Move down to bottom of line
    right(right)
    forward(forward1)

    # Left Leg
    left(left)
    forward(forward2)
    back(back)

def hrleg(right, forward, back):
    # Right leg
    right(right)
    forward(forward)
    back(back)

wordlist = ["dog", "cat", "michigan", "rick wash", "hangman"]

randomword = random.choice(wordlist)

wordfill = list('_' * (len(randomword)))



while True:

    print("Guess the following word: ", wordfill)

    userletter = input("Guess a letter in the word! ")

    if userletter == "":
        break

    letterindex = randomword.find(userletter)


    if letterindex == -1:
        #print("wrong")

        if counter == 0:
            hbody()
            print("body")

        if counter == 1:
            hhead()
            print("head")

        if counter == 2:
            hrarm()
            print("rarm")

        if counter == 3:
            hlarm()
            print("larm")

        if counter == 4:
            hlleg()
            print("lleg")

        if counter == 5:
            hrleg()
            print("rleg")

        elif counter >= 5:
            print("Game Over!")

            break

        counter += 1

    if letterindex != -1:
        print("correct")

        wordfill[letterindex] = userletter


hbody(90, 75, 75)
hhead(90, 25, 90)
hrarm(25, 45, 45, 45)
hlarm(90, 45, 45)
hlleg(45, 50, 30 ,70, 70)
hrleg(60, 70, 70)

# Wait for the user to close the window
done()
