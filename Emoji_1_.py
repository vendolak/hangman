from turtle import *


r = 100

#Rectangle Base
penup()
forward(125)    #1/2 length = 125
right(90)       #arrow pointing down
forward(30)     #height = 30
right(90)       #arrow pointing left

#at bottom right corner of block
width(1.5)
pendown()
fillcolor("brown")
begin_fill()
forward(250)    #length = 250
right(90)
forward(250)    #height = 250
right(90)
forward(250)
right(90)
forward(250)
end_fill()
penup()

#Resituate for circle
left(180)       #arrow pointing up
forward(30)
left(90)        #arrow pointing left
forward(125)
right(180)      #arrow pointing right


#Make Clock circle
width(1.5)
pendown()
pencolor("black")
begin_fill()
fillcolor("yellow")
circle(r)
end_fill()
penup()

#repostition to center
left(90)
forward(100)

#minute hand
width(4)
pendown()
pencolor("black")
right(30)
forward(75)
back(75)

#hour hand
right(120)
forward(55)
back(55)
penup()

#Inner tiny circle
#reposition arrow
right(30)   #arrow pointing down
right(90)   #arrow pointing left
forward(10) #same radius as circle
left(90)    #arrow pointing down

pendown()
begin_fill()
color("black")
circle(10)


end_fill()
penup()

#

#draw plane

def plane():
    #plane shape
    pendown()
    left(30)
    forward(100)
    right(130)
    forward(110)
    right(120)
    forward(90)
    left(180)
    forward(45)
    left(95)
    forward(95)
    right(180)
    forward(95)
    left(45)
    forward(25)
    left(130)
    forward(17)
    penup()
    left(90)
    forward(20)
    left(90)
    forward(20)

    #lines
    pendown()
    forward(5)
    penup()
    forward(5)
    pendown()
    forward(5)
    penup()
    forward(5)
    pendown()
    forward(5)
    penup()
    forward(5)
    pendown()
    forward(5)
    penup()
    right(30)
    pendown()
    forward(5)
    penup()
    forward(5)
    right(30)
    pendown()
    forward(5)
    penup()
    forward(5)
    right(30)
    pendown()
    forward(5)
    penup()
    forward(5)
    right(30)
    pendown()
    forward(5)
    penup()
    forward(5)
    right(30)
    pendown()
    forward(5)
    penup()
    forward(5)
    right(30)
    pendown()
    forward(5)
    penup()
    forward(5)
    right(30)
    pendown()
    forward(5)
    penup()
    forward(5)
    right(30)
    pendown()
    forward(5)
    penup()
    forward(5)
    right(30)
    pendown()
    forward(5)
    penup()
    forward(5)
    right(30)
    pendown()
    forward(5)
    penup()
    forward(5)
    right(30)
    pendown()
    forward(5)
    penup()
    forward(5)
    right(30)
    pendown()
    forward(5)
    penup()
    forward(5)
    pendown()
    forward(5)
    penup()
    forward(5)
    pendown()
    forward(5)
    penup()
    forward(5)
    pendown()
    forward(5)
    penup()
    forward(5)
    pendown()
    forward(5)
    penup()
    forward(5)
    pendown()
    forward(5)

penup()
right(60)
pendown()
plane()


done()
