import random
from turtle import *

#hangman drawing 
counter = 0

t2 = clone()

t2.color("red")
t2.penup()
t2.left(90)
t2.forward(100)
t2.right(90)
t2.pendown()

#forward(100)

def hbody():
    right(90)

    # Draw a line
    forward(75)

    # Go back to the center
    back(75)
    
def hhead():
    # Draw a circle for a head
    left(90)
    circle(25)
    right(90)

def hrarm():
    # Move down the line
    forward(25)

    # Draw right arm
    right(45)
    forward(45)
    back(45)

def hlarm():
    # Draw left arm
    left(90)
    forward(45)
    back(45)

def hlleg():
    # Move down to bottom of line
    right(45)
    forward(50)

    # Left Leg
    left(30)
    forward(70)
    back(70)

def hrleg():
    # Right leg
    right(60)
    forward(70)
    back(70)

wordlist = ["dog", "cat", "michigan", "rick wash", "hangman"]

randomword = random.choice(wordlist)

wordfill = list('_' * (len(randomword)))

while True:
    t2.penup()
    t2.forward(150)
    t2.pendown()
    
    #print("Guess the following word: ", wordfill)
    t2.write("Guess the following word: ")
    t2.penup()
    t2.forward(150)
    t2.pendown()
    t2.write(wordfill)
    
    #userletter = input("Guess a letter in the word! ")
    userletter = textinput("Input a letter", "Guess a letter in the word! ")
    
    if userletter == "":
        break

    letterindex = randomword.find(userletter)
    
    if letterindex == -1:
        #print("wrong")

        if counter == 0:
            hbody()
            #print("body")
            
        if counter == 1:
            hhead()
            #print("head")
            
        if counter == 2:
            hrarm()
            #print("rarm")
            
        if counter == 3:
            hlarm()
            #print("larm")
            
        if counter == 4:
            hlleg()
            #print("lleg")
            
        if counter == 5:
            hrleg()
            #print("rleg")
            
        elif counter >= 5:
            print("Game Over!")
            
            break
            
        counter += 1
    
    if letterindex != -1:
        print("correct")
        
        wordfill[letterindex] = userletter

#    for letter in randomword:

        # if letter == userletter:
        
            # letterindex = userletter.find(letter)
            
            # wordfill[letterindex] = userletter
            
        # else:
            # if counter == 0:
                # hbody()
                # print("body")
                
            # if counter == 1:
                # hhead()
                # print("head")
                
            # if counter == 2:
                # hrarm()
                # print("rarm")
                
            # if counter == 3:
                # hlarm()
                # print("larm")
                
            # if counter == 4:
                # hlleg()
                # print("lleg")
                
            # if counter == 5:
                # hrleg()
                # print("rleg")
                
            # elif counter >= 5:
                # print("Game Over!")
                
                # break
                
            # counter += 1
            
# hbody()
# hhead()
# hrarm()
# hlarm()
# hlleg()
# hrleg()

# Wait for the user to close the window
#done()
