import random
from turtle import *

#hangman drawing
counter = 0

t2 = clone()

t2.color("red")
t2.penup()
t2.left(90)
t2.forward(100)
t2.right(90)
t2.pendown()

#forward(100)

def hbody():
    right(90)

    # Draw a line
    forward(75)

    # Go back to the center
    back(75)

def hhead():
    # Draw a circle for a head
    left(90)
    circle(25)
    right(90)

def hrarm():
    # Move down the line
    forward(25)

    # Draw right arm
    right(45)
    forward(45)
    back(45)

def hlarm():
    # Draw left arm
    left(90)
    forward(45)
    back(45)

def hlleg():
    # Move down to bottom of line
    right(45)
    forward(50)

    # Left Leg
    left(30)
    forward(70)
    back(70)

def hrleg():
    # Right leg
    right(60)
    forward(70)
    back(70)

wordlist = ["dog", "cat", "michigan", "rick wash", "hangman"]

randomword = random.choice(wordlist)

wordfill = list('_' * (len(randomword)))

while True:
    t2.penup()
    t2.forward(150)
    t2.pendown()

    #print("Guess the following w
